import { defineConfig } from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
  publicDir: 'src/assets',
  base: '/bibliotecas/wc/govbr-ds-wc-quickstart-js/',
})
