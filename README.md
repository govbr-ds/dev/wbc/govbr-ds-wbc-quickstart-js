# Web Components GovBR-DS - Quickstart JS

## Descrição

Projeto exemplificando o uso da [biblioteca de Web Components do GovBR-DS](https://gov.br/ds/webcomponents 'Biblioteca de Web Components do GovBR-DS') em projetos JavaScript.

[Visualização ao vivo](https://govbr-ds.gitlab.io/bibliotecas/wc/govbr-ds-wc-quickstart-js/).

## O que é um quickstart?

É um projeto modelo pré-configurado que facilita o início das atividades assim como serve de exemplo para projetos já em andamento.

## Considerações sobre segurança, qualidade e boas práticas

É importante entender que um projeto Quickstart em software não deve ser usado diretamente em um ambiente de produção. Isso porque esses projetos são simplificados e podem não lidar com todos os desafios do mundo real.

O código do projeto Quickstart pode não lidar com questões avançadas, como segurança, escalabilidade ou gerenciamento de erros. Portanto, antes de implantar o código em um ambiente de produção real, **é crucial revisar**, testar e personalizar o código para atender às necessidades específicas do projeto e garantir que seja robusto e seguro.

Além disso, em projetos Quickstart de software, às vezes são tomadas liberdades na forma como o código é escrito para torná-lo mais fácil de entender. Isso pode significar que algumas boas práticas de programação não são seguidas ou que o código não está otimizado para desempenho máximo. Portanto, é responsabilidade do desenvolvedor adaptar o projeto Quickstart para atender às necessidades e padrões de produção adequados.

Em resumo, um projeto Quickstart em software é **um ponto de partida** útil, mas não deve ser implantado diretamente em um ambiente de produção sem revisão e ajustes adequados. Os desenvolvedores devem lembrar que a simplicidade é frequentemente priorizada em projetos Quickstart em detrimento da complexidade do mundo real e devem personalizar o código para atender às necessidades específicas de seu projeto.

## Tecnologias

Esse projeto é desenvolvido usando:

1. [Biblioteca de Web Components do GovBR-DS](https://gov.br/ds/webcomponents 'Biblioteca de Web Components do GovBR-DS')
1. JS
1. HTML

Para saber mais detalhes sobre Web Components sugerimos que consulte o [MDN](https://developer.mozilla.org/pt-BR/docs/Web/Web_Components 'Web Components | MDN').

## Dependências

As principais dependências do projeto são:

1. [GovBR-DS](https://www.gov.br/ds/ 'GovBR-DS')

1. [Web Components](https://gov.br/ds/webcomponents/ 'Web Components GovBR-DS')

1. [Font Awesome](https://fontawesome.com/ 'Font Awesome')

1. [Fonte Rawline](https://www.cdnfonts.com/rawline.font/ 'Fonte Rawline')

> O fontawesome e a fonte rawline podem ser importadas de um CDN. Consulte a documentação no site do GovBR-DS para mais detalhes

## Como executar o projeto?

```sh
git clone git@gitlab.com:govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js.git

npm install
```

Depois de executar os comando, abra o arquivo *src/index.html*.

OBS: Para contribuir com o projeto o clone pode não ser a maneira correta. Por favor consulte nossos guias sobre como contribuir na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').

### Explicando

Para usar os Web Components GovBR-DS com JavaScript e HTML é necessário:

- Importar o fontawesome, fonte rawline e o CSS do GovBR-DS

```html
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />

<link rel="stylesheet" href="https://cdngovbr-ds.estaleiro.serpro.gov.br/design-system/fonts/rawline/css/rawline.css" />

<link rel="stylesheet" href="../node_modules/@govbr-ds/core/dist/core.min.css" />
```

- Importar a biblioteca de Web Components

```html
<script src="../node_modules/@govbr-ds/webcomponents/dist/webcomponents.umd.min.js"></script>
```

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do GovBR-DS [http://gov.br/ds](http://gov.br/ds)

- Web Components [https://gov.br/ds/webcomponents/](https://gov.br/ds/webcomponents/)

- Usando nosso canal no discord [https://discord.gg/U5GwPfqhUP](https://discord.gg/U5GwPfqhUP)

## Como contribuir?

Antes de abrir um Merge Request tenha em mente algumas informações:

- Esse é um projeto opensource e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, escolha um título curto, simples e explicativo para o MR, e siga os padrões da nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir com o projeto? Confira o nosso guia [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

### Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender sobre os nossos padrões.

## Créditos

Os Web Components do GovBR-DS são criados pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') e [Dataprev](https://www.dataprev.gov.br/ 'Dataprev | Empresa de Tecnologia e Informações da Previdência') juntamente com a participação da comunidade.

## Licença

Nesse projeto usamos a licença MIT.
