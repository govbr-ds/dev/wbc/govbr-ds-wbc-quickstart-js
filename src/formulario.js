;(function () {
  const user = {
    name: '',
    email: '',
    mobile: '',
    password: '',
    rg: '',
    cpf: '',
    birthDate: '',
    address: '',
    cep: '',

    showError: function (element, msg) {
      let next = element.nextSibling.nextSibling
      element.state = 'danger'
      next.style.display = 'block'
      next.textContent = msg
    },
    fixError: function (element) {
      let next = element.nextSibling.nextSibling
      next.style.display = 'none'
      element.state = ''
    },
    onName: function (element, me, evt) {
      let shadow = element.shadowRoot
      let _element = shadow.getElementById(element.id)
      let value = _element.value
      me[element.id] = value
      me.onValidateName(element, value)
    },
    onValidateName: function name(element, value) {
      if (!value) {
        this.showError(element, 'Nome é obrigatório')
      } else {
        this.fixError(element)
      }
    },
    onValidateMobile: function (element, value) {
      if (!value) {
        this.showError(element, 'Telefone é obrigatório')
      } else {
        this.fixError(element)
      }
    },
    onMobile: function (element, me, evt) {
      let shadow = element.shadowRoot
      let _element = shadow.getElementById(element.id)
      let value = _element.value
      me.mobile = value
      me.onValidateMobile(element, value)
    },
    onCpf: function (element, me, evt) {
      let shadow = element.shadowRoot
      let _element = shadow.getElementById(element.id)
      let value = _element.value
      me.cpf = value
      me.onValidateCpf(element, value)
    },
    onValidateCpf: function (element, value) {
      let shadow = element.shadowRoot
      let _element = shadow.getElementById(element.id)
      value = _element.value
      if (!value) {
        this.showError(element, 'É obrigatório informar o CPF')
      } else {
        this.fixError(element)
      }
    },
    onEmail: function (element, me, evt) {
      let shadow = element.shadowRoot
      let _element = shadow.getElementById(element.id)
      let value = _element.value
      me[element.id] = value
      me.onValidateEmail(element, value)
    },
    onValidateEmail: function name(element, value) {
      if (!value) {
        this.showError(element, 'Email é obrigatório')
      } else {
        this.fixError(element)
      }
    },
    onPassword: function (element, me, evt) {
      let shadow = element.shadowRoot
      let _element = shadow.getElementById(element.id)
      let value = _element.value
      me[element.id] = value
      me.onValidatePassword(element, value)
    },
    onValidatePassword: function name(element, value) {
      if (!value) {
        this.showError(element, 'Senha é obrigatória')
      } else {
        this.fixError(element)
      }
    },
    onRG: function (element, me, evt) {
      let shadow = element.shadowRoot
      let _element = shadow.getElementById(element.id)
      let value = _element.value
      me[element.id] = value
      me.onValidateRG(element, value)
    },
    onValidateRG: function name(element, value) {
      if (!value) {
        this.showError(element, 'RG é obrigatório')
      } else {
        this.fixError(element)
      }
    },
    onBirthDate: function (element, me, evt) {
      let shadow = element.shadowRoot
      let _element = shadow.getElementById(element.id)
      let value = _element.value
      me[element.id] = value
      me.onValidateBirthDate(element, value)
    },
    onValidateBirthDate: function name(element, value) {
      if (!value) {
        this.showError(element, 'Data de nascimento é obrigatório')
      } else {
        this.fixError(element)
      }
    },
    onAddress: function (element, me, evt) {
      let shadow = element.shadowRoot
      let _element = shadow.getElementById(element.id)
      let value = _element.value
      me[element.id] = value
    },
    onCep: function (element, me, evt) {
      let shadow = element.shadowRoot
      let _element = shadow.getElementById(element.id)
      let value = _element.value
      me[element.id] = value
    },
  }

  let userList = []
  let name = document.getElementById('name')
  let email = document.getElementById('email')
  let mobile = document.getElementById('mobile')
  let password = document.getElementById('password')
  let rg = document.getElementById('rg')
  let cpf = document.getElementById('cpf')
  let birthDate = document.getElementById('birthDate')
  let address = document.getElementById('address')
  let cep = document.getElementById('cep')
  let enviar = document.getElementById('enviar')

  const listPlaceholder = document.getElementById('user-list-placeholder')

  name.addEventListener('blur', user.onName.bind(this, name, user))
  email.addEventListener('blur', user.onEmail.bind(this, email, user))
  mobile.addEventListener('blur', user.onMobile.bind(this, mobile, user))
  password.addEventListener('blur', user.onPassword.bind(this, password, user))
  rg.addEventListener('blur', user.onRG.bind(this, rg, user))
  cpf.addEventListener('blur', user.onCpf.bind(this, cpf, user))
  birthDate.addEventListener('blur', user.onBirthDate.bind(this, birthDate, user))
  address.addEventListener('blur', user.onAddress.bind(this, address, user))
  cep.addEventListener('blur', user.onCep.bind(this, cep, user))

  enviar.addEventListener('click', (evt) => {
    user.onValidateName(name, user.name)
    user.onValidateEmail(email, user.email)
    user.onValidateMobile(mobile, user.mobile)
    user.onValidatePassword(password, user.password)
    user.onValidateRG(rg, user.rg)
    user.onValidateCpf(cpf, user.cpf)
    user.onValidateBirthDate(birthDate, user.birthDate)

    if (user.name && user.email && user.mobile && user.password && user.rg && user.cpf && user.birthDate) {
      toogleMessage('Formulário enviado com sucesso!', 'success')
      userList.push({
        name: user.name,
        email: user.email,
        mobile: user.mobile,
        password: user.password,
        rg: user.rg,
        cpf: user.cpf,
        birthDate: user.birthDate,
        address: user.address,
        cep: user.cep,
      })
      updateUserList()
    }
  })

  function toogleMessage(message, state) {
    const messageElement = document.getElementById('message')

    messageElement.setAttribute('state', state)
    messageElement.innerHTML = message
    messageElement.style.display = 'block'

    setTimeout(() => {
      messageElement.setAttribute('state', '')
      messageElement.innerHTML = ''
      messageElement.style.display = 'none'
    }, 10000)
  }

  function removeUser(cpf) {
    userList = userList.filter((user) => user.cpf !== cpf)
    console.log(userList)
    updateUserList()
  }

  function createUserList() {
    const h2 = document.createElement('h2')
    h2.innerHTML = 'Usuários Cadastrados'
    listPlaceholder.appendChild(h2)

    const parentList = document.createElement('br-list')
    parentList.setAttribute('title', 'Usuários')
    parentList.setAttribute('id', 'submited-users')
    parentList.setAttribute('data-toggle', 'true')
    listPlaceholder.appendChild(parentList)

    for (const user of userList) {
      const parentListItem = document.createElement('br-item')
      parentListItem.setAttribute('title', user.name)
      parentListItem.setAttribute('hover', '')

      const userList = document.createElement('br-list')
      const userListItem = document.createElement('br-item')
      userListItem.setAttribute('hover', '')

      const divItemsRow = document.createElement('div')
      divItemsRow.className = 'row align-items-center'
      const divItemsCol = document.createElement('div')
      divItemsCol.className = 'col-11'

      const emailP = document.createElement('p')
      emailP.textContent = `E-mail: ${user.email}`
      divItemsCol.appendChild(emailP)

      const mobileP = document.createElement('p')
      mobileP.textContent = `Telefone Celular: ${user.mobile}`
      divItemsCol.appendChild(mobileP)

      const passwordP = document.createElement('p')
      passwordP.textContent = `Senha: ************`
      divItemsCol.appendChild(passwordP)

      const rgP = document.createElement('p')
      rgP.textContent = `RG: ${user.rg}`
      divItemsCol.appendChild(rgP)

      const cpfP = document.createElement('p')
      cpfP.textContent = `CPF: ${user.cpf}`
      divItemsCol.appendChild(cpfP)

      const birthDateP = document.createElement('p')
      birthDateP.textContent = `Data de Nascimento: ${user.birthDate}`
      divItemsCol.appendChild(birthDateP)

      if (user.address) {
        const addressP = document.createElement('p')
        addressP.textContent = `Endereço: ${user.address}`
        divItemsCol.appendChild(addressP)
      }

      if (user.cep) {
        const cepP = document.createElement('p')
        cepP.textContent = `CEP: ${user.cep}`
        divItemsCol.appendChild(cepP)
      }

      divItemsRow.appendChild(divItemsCol)

      const divButtonCol = document.createElement('div')
      divButtonCol.className = 'col'
      const brButton = document.createElement('br-button')
      brButton.setAttribute('circle', true)
      brButton.setAttribute('icon', 'trash')
      brButton.setAttribute('type', 'primary')
      brButton.setAttribute('aria-labelledby', 'Remover Usuário')
      brButton.addEventListener('click', () => {
        removeUser(user.cpf)
      })

      divButtonCol.appendChild(brButton)
      divItemsRow.appendChild(divButtonCol)

      userListItem.appendChild(divItemsRow)
      userList.appendChild(userListItem)
      parentListItem.appendChild(userList)
      parentList.appendChild(parentListItem)
    }
  }

  function updateUserList() {
    deleteUserList()
    if (userList.length > 0) createUserList()
  }

  function deleteUserList() {
    listPlaceholder.innerHTML = ''
  }
})()
